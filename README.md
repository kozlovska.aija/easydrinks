Django files
/website/*
manage.py
in /website/settings.py for connecting to database in case of changes in db credentials have to be updated

Django container files for Docker image named web in docker-compose.yml
requirements.txt
Dockerfile

Docker configuration and setup
docker-compose.yml
.env

Data storage for Docker containers (is permanent and ignored by git)
/volumes/*

for database to init
https://hub.docker.com/_/postgres
Arbitrary --user Notes
third option 

3. initialize the target directory separately from the final runtime (with a chown in between):

$ docker volume create pgdata
$ docker run -it --rm -v pgdata:/var/lib/postgresql/data postgres
The files belonging to this database system will be owned by user "postgres".
...
( once it's finished initializing successfully and is waiting for connections, stop it )
$ docker run -it --rm -v pgdata:/var/lib/postgresql/data bash chown -R 1000:1000 /var/lib/postgresql/data
$ docker run -it --rm --user 1000:1000 -v pgdata:/var/lib/postgresql/data postgres
LOG:  database system was shut down at 2017-01-20 00:03:23 UTC
LOG:  MultiXact member wraparound protections are now enabled
LOG:  autovacuum launcher started
LOG:  database system is ready to accept connections