from django.db import models
from django.utils.safestring import mark_safe
from django.conf import settings
# for image resizing use imagekit
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Adjust

from categories.models import edCategory
from users.models import User

# ===================== Models ===================== #
#base sortiment - just products with base information and image    
class edSortiment(models.Model):
    EAN = models.CharField("Barcode (identifies product)", max_length=50)
    name = models.CharField("Name", max_length=200)
    percent = models.FloatField("Alcoholic drink strength percentage", null=True, blank=True, default=None)
    liters = models.FloatField("Volume in liters", null=True, blank=True, default=None)
    country = models.CharField("Country of origin", max_length=50, null=True, blank=True, default=None)
    edcategory = models.ForeignKey(
        edCategory,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='category',
    )
    photo = models.ImageField(upload_to='products', default='products/default.jpg')
    # technical fields 
    added_date = models.DateTimeField("Date added", auto_now_add=True, null=True)
    modified_date = models.DateTimeField("Last modified date", auto_now=True, null=True)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1,
    )
    #thimbnail image, virtual field, it is not really kept in database
    photo_thumbnail = ImageSpecField( source='photo',
                                      processors=[Adjust(contrast=1.2, sharpness=1.1),ResizeToFill(115, 249)],
                                      format='JPEG',
                                      options={'quality': 90}
                                    )
    photo_thumbnail_mini = ImageSpecField( source='photo',
                                      processors=[Adjust(contrast=1.2, sharpness=1.1),ResizeToFill(20, 20)],
                                      format='JPEG',
                                      options={'quality': 90}
                                    )
    
    def __str__(self):
        #return self.name
        return ('%s %s (%s)' % (self.name or '', self.EAN or '', self.edcategory.name or ''))

    def photo_tag(self):
        """
        Generate html for showing thumbnail photo (normal size).
        """
        return mark_safe('<img src="%s%s" />' % (settings.MEDIA_URL, self.photo))
    photo_tag.short_description = 'Photo'
    
    def preview(self):
        """
        Generate html for showing thumbnail photo with link to the real one.
        """
        if not self.photo_thumbnail:
            return ''
        # check if file exists   
        #if os.path.isfile(settings.MEDIA_URL + self.photo.url):
        return mark_safe('<a href="%s%s"><img src="%s" /></a>' % (settings.MEDIA_URL, self.photo, self.photo_thumbnail.url))
        #else:
        #    return ''
        #return mark_safe('<a href="{s.photo.url}" title="{s.name}"><img src="{thumb.url}" width="{thumb.width}" height="{thumb.height}" alt="{s.title}" /></a>'.format(s=self, thumb=self.photo_thumbnail))
        #return self.photo_thumbnail.url
    preview.short_description = 'preview'
    
    def preview_mini(self):
        """
        Generate html for showing thumbnail photo in a very small size with link to the real one.
        """
        if not self.photo_thumbnail_mini:
            return ''
        # check if file exists   
        #if os.path.isfile(settings.MEDIA_URL + self.photo.url):
        return mark_safe('<a href="%s%s"><img src="%s" /></a>' % (settings.MEDIA_URL, self.photo, self.photo_thumbnail_mini.url))
        #else:
        #    return ''
    preview.short_description = 'preview'
