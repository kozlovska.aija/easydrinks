from django.contrib import admin
from .models import edSortiment
from categories.models import edCategory
from import_export import resources
from import_export.fields import Field
from import_export.admin import ImportExportModelAdmin
from import_export.widgets import ForeignKeyWidget
from import_export import widgets
# for checking if image exists before returning image tag
import os, os.path
import fnmatch
from django.conf import settings
# for filter to find products without images
from django.contrib.admin import SimpleListFilter


# custom filter to find products with and without images
class imgExistenceFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'image existence'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'hasimg'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        #photos = set([c.photo for c in model_admin.model.objects.all()])
        return [('1', 'With Image')] + [
          ('2', 'Missing Image')]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == '2':
            return queryset.filter(photo='products/default.jpg')
        if self.value():
            return queryset.exclude(photo='products/default.jpg')

# Customize to create the new category value in case it does not exist 
class edCategoryWidget(widgets.ForeignKeyWidget):
    def clean(self, value, row=None, *args, **kwargs):
        return self.model.objects.get_or_create(name = value)[0]

# resource for file import 
class edSortimentResource(resources.ModelResource):
    name = Field(attribute='name', column_name='nosaukums')
    percent = Field(attribute='percent', column_name='procenti')
    liters = Field(attribute='liters', column_name='tilpums')
    edcategory = Field(column_name='kategorija', attribute='edcategory', widget=edCategoryWidget(edCategory, 'name'))
    
    #'preview', 'photo', 'EAN', 'name', 'edcategory',  
    #          'percent', 'liters', 
    #          'owner', 'added_date', 'modified_date'
    
    class Meta:
        model = edSortiment
        skip_unchanged = True
        report_skipped = False
        import_id_fields = ('EAN',)
        fields = ('name', 'percent', 'EAN', 'liters', 'edcategory' )



class edSortimentAdmin(ImportExportModelAdmin):
    resource_class = edSortimentResource
    pass
    list_display = ('name', 'edcategory' ,'preview_mini', 'EAN')
    list_filter = (imgExistenceFilter,  'edcategory',#'edShop_id__name'
    )
    save_on_top = True
    search_fields = ['EAN', 'name']
    fields = ['preview', 'photo', 'EAN', 'name', 'edcategory',  
              'percent', 'liters', 
              'owner', 'added_date', 'modified_date'
              ]
    readonly_fields = ['preview', 'owner', 'added_date', 'modified_date']
    #update image urls from the directory in server
    def update_images(self, request, queryset):
        for obj in queryset:
            photo_found=False
            # search for image file , it's name has to be EAN number
            for file in fnmatch.filter(os.listdir(os.path.join(settings.BASE_DIR, settings.MEDIA_ROOT, "products")), obj.EAN+'.*'):
                obj.photo = os.path.join("products", file)
                obj.save()
                #self.message_user(request, "Update path for "+obj.EAN+" to " + os.path.join(settings.MEDIA_URL, "products", file))
                photo_found=True
            #if photo was not found set it to default
            if not photo_found:
                obj.photo = 'products/default.jpg'
                obj.save()
        self.message_user(request, "Images updated")
    update_images.short_description = "Update images for products (after copying files to server)"

    actions = [update_images]
    
admin.site.register(edSortiment, edSortimentAdmin)

