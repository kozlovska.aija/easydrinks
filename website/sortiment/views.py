from django.shortcuts import render
from django.views import generic
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# include main model for shops
from .models import edSortiment

#####   Sortiment Views
class edSortimentListView(generic.ListView):
    model = edSortiment
    context_object_name = 'sortiment'   # your own name for the list as a template variable
    template_name = 'sortiment/list.html'  # Specify your own template name/location

class edSortimentDetailView(generic.DetailView):
    model = edSortiment   
    context_object_name = 'sortiment'   # your own name for the list as a template variable
    template_name = 'sortiment/detail.html'  # Specify your own template name/location 

class edSortimentCreateView(CreateView):
    model = edSortiment
    context_object_name = 'sortiment'   # your own name for the list as a template variable
    template_name = 'sortiment/new.html'
    fields = ['EAN', 'name', 'percent', 'liters', 'country', 'edcategory', 'photo']
    success_url = reverse_lazy('sortiment:view')
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(edSortimentCreateView, self).form_valid(form)

class edSortimentUpdateView(UpdateView):
    model = edSortiment
    template_name = 'sortiment/edit.html'
    context_object_name = 'sortiment'   # your own name for the list as a template variable
    fields = ['photo', 'edcategory', 'EAN', 'name', 'percent', 'liters', 'country']
    success_url = reverse_lazy('sortiment:view')

class edSortimentDeleteView(DeleteView):
    model = edSortiment
    #template_name = 'stores/edSortiment_confirm_delete.html'
    success_url = reverse_lazy('sortiment:view')

