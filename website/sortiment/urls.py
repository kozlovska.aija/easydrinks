from django.urls import path
from django.contrib.auth.decorators import login_required

# include views for rendering the output
from .views import edSortimentListView
from .views import edSortimentCreateView
from .views import edSortimentUpdateView
from .views import edSortimentDeleteView

app_name = 'sortiment'

urlpatterns = [
    path('sortiment/', login_required(edSortimentListView.as_view()), name='view'),
    path('sortiment/new/', login_required(edSortimentCreateView.as_view()), name='new'),
    path('sortiment/<int:pk>', login_required(edSortimentUpdateView.as_view()), name='edit'),
    path('sortiment/<int:pk>/delete/', login_required(edSortimentDeleteView.as_view()), name='delete'),
]

