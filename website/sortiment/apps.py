from django.apps import AppConfig


class SortimentConfig(AppConfig):
    name = 'sortiment'
