from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User

class UserAdmin(BaseUserAdmin):
    model = User
    BaseUserAdmin.fieldsets += (
        (('Additional info'), {'fields': ('company', 'address', 'phone')}),
    )
    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'company', 'phone')
    class Meta:
        model = User

admin.site.register(User, UserAdmin)