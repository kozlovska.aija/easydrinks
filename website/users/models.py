from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    company = models.CharField("Company", max_length=200, default=None, blank=True, null=True)
    address = models.CharField("Address", max_length=4000, default=None, blank=True, null=True)
    phone = models.CharField("Phone", max_length=200, default=None, blank=True, null=True)
    pass