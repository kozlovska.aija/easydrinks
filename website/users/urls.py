from django.conf.urls import url, include

from django.conf import settings
from django.contrib.staticfiles import views
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views

from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

app_name = 'users'

urlpatterns = [
    url(r'^users/login/$', auth_views.login, {'template_name': 'users/login.html'}, name='login'),
    url(r'^users/logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^users/profile/$', login_required(TemplateView.as_view(template_name='users/profile.html')), name='profile'),
    url(r'^users/shopsortiment/$', login_required(TemplateView.as_view(template_name='users/sortiment.html')), name='shopsortiment'),
    url(r'^users/sale/$', login_required(TemplateView.as_view(template_name='users/sale.html')), name='sale'),
    path('users/', include('django.contrib.auth.urls')), #add standard urls for authentication
    # ...
]

