from django.shortcuts import render
from django.views import generic
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# include main model for shops
from .models import edShop

class edShopListView(generic.ListView):
    model = edShop
    context_object_name = 'shop'   # your own name for the list as a template variable
    template_name = 'shops/list.html'  # Specify your own template name/location

class edShopDetailView(generic.DetailView):
    model = edShop   
    context_object_name = 'shop'   # your own name for the list as a template variable
    template_name = 'shops/detail.html'  # Specify your own template name/location 

class edShopCreateView(CreateView):
    model = edShop
    context_object_name = 'shop'   # your own name for the list as a template variable
    template_name = 'shops/new.html'
    fields = ['name', 'code', 'city', 'street', 'number', 'postal_code']
    success_url = reverse_lazy('shops:view')
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(edShopCreateView, self).form_valid(form)

class edShopUpdateView(UpdateView):
    model = edShop
    template_name = 'shops/edit.html'
    context_object_name = 'shop'   # your own name for the list as a template variable
    fields = ['name', 'code', 'city', 'street', 'number', 'postal_code']
    success_url = reverse_lazy('shops:view')

class edShopDeleteView(DeleteView):
    model = edShop
    #template_name = 'stores/edShop_confirm_delete.html'
    success_url = reverse_lazy('shops:view')
