from django.urls import path
from django.contrib.auth.decorators import login_required

# include shops views for rendering the output
from .views import edShopListView
from .views import edShopCreateView
from .views import edShopUpdateView
from .views import edShopDeleteView

app_name = 'shops'

urlpatterns = [
    path('shop/', login_required(edShopListView.as_view()), name='view'),
    path('shop/new/', login_required(edShopCreateView.as_view()), name='new'),
    path('shop/<int:pk>', login_required(edShopUpdateView.as_view()), name='edit'),
    path('shop/<int:pk>/delete/', login_required(edShopDeleteView.as_view()), name='delete'),
]

