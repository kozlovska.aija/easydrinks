from django.db import models
from users.models import User

# Create your models here.
# shops classificator
class edShop(models.Model):
    name = models.CharField("Shop name", max_length=200)
    city = models.CharField("City", max_length=50)
    street = models.CharField("Street", max_length=100)
    number = models.CharField("Houseumber", max_length=10)
    postal_code = models.CharField("Postal code", max_length=10)
    code = models.CharField("Shop ID Code", max_length=50, null=True, blank=True, default=None)
    #
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1,
    )
    added_date = models.DateTimeField("Date added", auto_now_add=True, null=True)
    modified_date = models.DateTimeField("Date last modified", auto_now=True, null=True)
    
    def __str__(self):
        return ('%s: %s (%s, %s %s, %s)' % (self.code or '', self.name or '', self.city or '', self.street or '', self.number or '', self.postal_code or ''))
