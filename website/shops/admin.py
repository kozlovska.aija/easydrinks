from django.contrib import admin

# Shop administration layout 
from .models import edShop
class edShopAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'name', 'code')
    list_filter = ('name','city')
    save_on_top = True
    search_fields = ['name', 'code', 'city']
    fields = [
        'name',
        'code',
        'city',
        'street',
        'number',
        'postal_code',
        'owner', 
        'added_date', 
        'modified_date'
    ]
    readonly_fields = ['owner', 'added_date', 'modified_date']

admin.site.register(edShop, edShopAdmin)
