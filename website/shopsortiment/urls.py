from django.urls import path
from django.contrib.auth.decorators import login_required

# include views for rendering the output
from .views import edShopSortimentListView
from .views import edShopSortimentCreateView
from .views import edShopSortimentUpdateView
from .views import edShopSortimentDeleteView

app_name = 'shopsortiment'

urlpatterns = [
    path('shopsortiment/', login_required(edShopSortimentListView.as_view()), name='view'),
    path('shopsortiment/new/', login_required(edShopSortimentCreateView.as_view()), name='new'),
    path('shopsortiment/<int:pk>', login_required(edShopSortimentUpdateView.as_view()), name='edit'),
    path('shopsortiment/<int:pk>/delete/', login_required(edShopSortimentDeleteView.as_view()), name='delete'),
]

