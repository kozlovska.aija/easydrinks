from django.contrib import admin
from .models import edShopSortiment
from import_export import resources
from import_export.fields import Field
from import_export.admin import ImportExportModelAdmin
from import_export.widgets import ForeignKeyWidget
from import_export import widgets
# for checking if image exists before returning image tag
import os, os.path
import fnmatch
from django.conf import settings
# for filter to find products without images
from django.contrib.admin import SimpleListFilter

class edShopSortimentAdmin(admin.ModelAdmin):
#    list_display = (#'name', #'preview_mini', 
#                    #'category_name', 
#                    'EAN',)
    #list_filter = ('edsortimentbase__category__name', #'edShop_id__name'
    #)
    save_on_top = True
#    search_fields = ['EAN']
    fields = ['sortiment', 'price_vat', 'price_no_vat',
              'shops', 
              'owner', 'added_date', 'modified_date'
              ]
    readonly_fields = ['owner', 'added_date', 'modified_date']
    def category_name(self, instance):
        return instance.edsortiment.edcategory

admin.site.register(edShopSortiment, edShopSortimentAdmin)
 