from django.shortcuts import render
from django.views import generic
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# include main model for shops
from .models import edShopSortiment

#####   Sortiment Views
class edShopSortimentListView(generic.ListView):
    model = edShopSortiment
    context_object_name = 'shopsortiment'   # your own name for the list as a template variable
    template_name = 'shopsortiment/list.html'  # Specify your own template name/location

class edShopSortimentDetailView(generic.DetailView):
    model = edShopSortiment   
    context_object_name = 'shopsortiment'   # your own name for the list as a template variable
    template_name = 'shopsortiment/detail.html'  # Specify your own template name/location 

class edShopSortimentCreateView(CreateView):
    model = edShopSortiment
    context_object_name = 'shopsortiment'   # your own name for the list as a template variable
    template_name = 'shopsortiment/new.html'
    fields = ['sortiment','shops','price_vat', 'price_no_vat', 'status', 'additional_info']
    success_url = reverse_lazy('shopsortiment:view')
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(edShopSortimentCreateView, self).form_valid(form)

class edShopSortimentUpdateView(UpdateView):
    model = edShopSortiment
    template_name = 'shopsortiment/edit.html'
    context_object_name = 'shopsortiment'   # your own name for the list as a template variable
    fields = ['sortiment','shops','price_vat', 'price_no_vat', 'status', 'additional_info']
    success_url = reverse_lazy('shopsortiment:view')

class edShopSortimentDeleteView(DeleteView):
    model = edShopSortiment
    #template_name = 'stores/edSortiment_confirm_delete.html'
    success_url = reverse_lazy('shopsortiment:view')

