from django.db import models
from django.utils.safestring import mark_safe
from django.conf import settings
# for image resizing use imagekit
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, Adjust

from sortiment.models import edSortiment
from shops.models import edShop
from users.models import User

# ===================== Models ===================== #
# shops sortiment with 
class edShopSortiment(models.Model):
    price_vat = models.FloatField("Price including VAT", null=True, blank=True, default=None)
    price_no_vat = models.FloatField("Price excluding VAT", null=True, blank=True, default=None)
    status = models.CharField("Status", max_length=50, null=True, blank=True, default=None)
    additional_info = models.CharField("Additional information", max_length=500, null=True, blank=True, default=None)
    # 
    added_date = models.DateTimeField("Date added", auto_now_add=True, null=True)
    modified_date = models.DateTimeField("Last modified date", auto_now=True, null=True)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1,
    )
    shops = models.ManyToManyField(edShop)
    sortiment = models.ForeignKey(
        edSortiment,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    
    def __str__(self):
        return self.sortiment.name
