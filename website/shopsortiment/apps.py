from django.apps import AppConfig


class ShopsortimentConfig(AppConfig):
    name = 'shopsortiment'
