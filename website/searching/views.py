# Create your views here.
from django.views.generic.edit import FormView
#from django.views.generic.list import ListView
#from django.shortcuts import render
#from django.http import HttpResponseRedirect
#from django.urls import reverse
#from django.views import generic
from haystack.generic_views import SearchView
from haystack.forms import SearchForm
#from haystack.views import SearchView
#from haystack.forms import SearchForm, ModelSearchForm

from .forms import edSearchForm

#use other template and class as the default one
class edMainView(SearchView):
    """Main page with search form"""
    template_name = 'searching/index.html'
    form_class=edSearchForm
