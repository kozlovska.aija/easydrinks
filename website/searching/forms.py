from django import forms
from haystack.forms import SearchForm

class edSearchForm(SearchForm):
    #don't use label for search fiels as there is only one
    #q = forms.CharField(label='', max_length=100)
    q = forms.CharField(required=False, label='', widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Labākās dzērienu cenas...'}))