import datetime
from haystack import indexes
from shopsortment.models import edShopSortiment

class edShopSortimentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    EAN = indexes.CharField(model_attr='EAN')
    name = indexes.CharField(model_attr='name')
    percent = indexes.FloatField(model_attr='percent', null=True)
    liters = indexes.FloatField(model_attr='liters', null=True)
    price_vat = indexes.FloatField(model_attr='price_vat', null=True)
    price_no_vat = indexes.FloatField(model_attr='price_no_vat', null=True)
    category = indexes.CharField(model_attr='category')
    country = indexes.CharField(model_attr='country', null=True)
    manufacturer = indexes.CharField(model_attr='manufacturer', null=True)
    photo = indexes.CharField(model_attr='photo')
    
    def get_model(self):
        return edShopSortiment

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()