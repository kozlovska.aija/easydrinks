from django.conf.urls import url, include

from django.conf import settings
from django.contrib.staticfiles import views
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include

from .views import edMainView

app_name = 'searching'
urlpatterns = [
    #main window
    #url(r'^$', views.index, name='index'),
    # ex: /searchform/hennesy/results/
    #url(r'^(?P<search_term>[a-z]+)/results/$', views.results, name='results'),
    
    url(r'^$', edMainView.as_view(), name='home'),
    
    #url(r'^search/', include('haystack.urls')),
#    url(r'^results-list/$', edSearchResultsListView.as_view(), name='results-list'),
] 

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
