from django.urls import path
from django.contrib.auth.decorators import login_required

# include views for rendering the output
from .views import edCategoryListView
from .views import edCategoryCreateView
from .views import edCategoryUpdateView
from .views import edCategoryDeleteView

app_name = 'categories'

urlpatterns = [
    path('category/', login_required(edCategoryListView.as_view()), name='view'),
    path('category/new/', login_required(edCategoryCreateView.as_view()), name='new'),
    path('category/<int:pk>', login_required(edCategoryUpdateView.as_view()), name='edit'),
    path('category/<int:pk>/delete/', login_required(edCategoryDeleteView.as_view()), name='delete'),
]

