from django.shortcuts import render
from django.views import generic
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# include main model for shops
from .models import edCategory

#####   Category Views
class edCategoryListView(generic.ListView):
    model = edCategory
    context_object_name = 'category'   # your own name for the list as a template variable
    template_name = 'categories/list.html'  # Specify your own template name/location

class edCategoryDetailView(generic.DetailView):
    model = edCategory   
    context_object_name = 'category'   # your own name for the list as a template variable
    template_name = 'categories/detail.html'  # Specify your own template name/location 

class edCategoryCreateView(CreateView):
    model = edCategory
    context_object_name = 'category'   # your own name for the list as a template variable
    template_name = 'categories/new.html'
    fields = ['name', 'parent_category']
    success_url = reverse_lazy('categories:view')
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(edCategoryCreateView, self).form_valid(form)

class edCategoryUpdateView(UpdateView):
    model = edCategory
    template_name = 'categories/edit.html'
    context_object_name = 'category'   # your own name for the list as a template variable
    fields = ['name', 'parent_category']
    success_url = reverse_lazy('categories:view')

class edCategoryDeleteView(DeleteView):
    model = edCategory
    #template_name = 'stores/edCategory_confirm_delete.html'
    success_url = reverse_lazy('categories:view')    
