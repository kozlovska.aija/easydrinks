from django.contrib import admin

from .models import edCategory

class edCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent_category')
    save_on_top = True
    search_fields = ['name', 'parent_category']
    fields = ['name', 'parent_category', 'owner', 'added_date', 'modified_date'
              ]
    readonly_fields = ['owner', 'added_date', 'modified_date']

admin.site.register(edCategory, edCategoryAdmin)