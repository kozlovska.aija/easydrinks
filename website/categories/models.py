from django.db import models
from users.models import User

# Create your models here.
# ===================== Models ===================== #
# product categories classificator
class edCategory(models.Model):
    name = models.CharField("Category name", max_length=200)
    parent_category = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    #technical fields
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=1,
    )
    added_date = models.DateTimeField("Date added", auto_now_add=True, null=True)
    modified_date = models.DateTimeField("Date last modified", auto_now=True, null=True)
    
    def __str__(self):
        return ('%s' % (self.name or ''))
