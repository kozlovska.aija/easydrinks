#!/bin/sh
SSH_DOCUMENT_ROOT="/home/aija/easydrinks"
DIR_DJANGO="website" #folder name for all python django apps
APP_NAME="easydrinks" #folder name for the django_app/ subfolder to keep the files
APP_IMG_NAME="web" #name of the container of Django app, as it is in docker-compose.yml file
APP_NAME_SEARCH="searching" #name of the search form app
APP_NAME_ACCOUNTS="accounts" #name of the accounts form app
PROXY_NETWORK="proxy" #network name 
USER_OWN="aija" #owner for new files/folders 

cd $SSH_DOCUMENT_ROOT # open dir where yml file is stored 
whoami
pwd
#at first some cleanup tasks (remove dangling images, volumes and exited containers)
if [[ $(docker ps -a -f status=exited -q) ]]; 
then
    echo "  ***  [INFO] Docker containers: removing exited containers  ***  "
    docker rm $(docker ps -a -f status=exited -q)
else
    echo "  ***  [OK]   Docker containers: no exited containers to remove  ***  "
fi
if [[ $(docker images -f dangling=true -q) ]]; 
then
    echo "  ***  [INFO] Docker images: removing dangling images  ***  "
    docker rmi $(docker images -f dangling=true -q)
else    
    echo "  ***  [OK]   Docker images: no dangling images to remove  ***  "
fi
if [[ $(docker volume ls -f dangling=true -q) ]];
then
    echo "  ***  [INFO] Docker volumes: removing dangling volumes  ***  "
    docker volume rm $(docker volume ls -f dangling=true -q)
else    
    echo "  ***  [OK]   Docker volumes: no dangling volumes to remove  ***  "
fi
echo "  ***  [INFO] Docker containers: stopping all  ***  "
docker-compose down || true #stop all running containers before installing changes

#create required network if it does not exist
if [ ! "$(docker network ls -q -f name=$PROXY_NETWORK)" ]; then
    echo "  ***  [INFO] Proxy network $PROXY_NETWORK does not exist. Creating it  ***  "
    docker network create $PROXY_NETWORK 
fi
#add git origin (remove previous one before that)
echo "  ***  [INFO] Git remote origin: remove and add again  ***  "
git remote rm origin
git remote add origin git@gitlab.com:kozlovska.aija/easydrinks.git
#fetch latest changes to server
echo "  ***  [INFO] Git: fetch latest changes from git  ***  "
git fetch origin 
git reset --hard origin/master

#allow everyone to access sass
chmod 777 -R sass/*
#if there are changes in Dockerfiles perform build
if git diff HEAD~ --name-only | grep -E "Dockerfile|requirements.txt|docker-compose.yml"; 
then
    echo "  ***  [INFO] Docker images: info changed, building  ***  "
    sudo docker-compose build --force-rm --no-cache
else
    echo "  ***  [INFO] Docker images: no changes  ***  "
fi

#start containers
echo "  ***  [INFO] Docker containers: start all  ***  "
docker-compose up -d

# if there is no required app in Django container, create it. 
# After that it is required to copy out the created files and add to git repository
if [[ $(ls -A $DIR_DJANGO/$APP_NAME) ]]; 
then
    echo "  ***  [OK]   Django project $APP_NAME: exists ***  "
else
    echo "  ***  [WARN] Django project $APP_NAME: not exists, create. Now add it to git!  ***  "
    docker-compose run ${APP_IMG_NAME} django-admin.py startproject $APP_NAME . 
    chown -R $USER_OWN:$USER_OWN $DIR_DJANGO/$APP_NAME
fi
# if there is no required app in Django container, create it. 
# After that it is required to copy out the created files and add to git repository
if [[ $(ls -A $DIR_DJANGO/$APP_NAME_SEARCH) ]]; 
then
    echo "  ***  [OK]   Django project $APP_NAME_SEARCH: exists ***  "
else
    echo "  ***  [WARN] Django project $APP_NAME_SEARCH: not exists, create. Now add it to git!  ***  "
    docker-compose run ${APP_IMG_NAME} django-admin.py startproject $APP_NAME_SEARCH . 
    chown -R $USER_OWN:$USER_OWN $DIR_DJANGO/$APP_NAME_SEARCH
fi
# if there is no required app in Django container, create it. 
# After that it is required to copy out the created files and add to git repository
if [[ $(ls -A $DIR_DJANGO/$APP_NAME_ACCOUNTS) ]]; 
then
    echo "  ***  [OK]   Django project $APP_NAME_ACCOUNTS: exists ***  "
else
    echo "  ***  [WARN] Django project $APP_NAME_ACCOUNTS: not exists, create. Now add it to git!  ***  "
    docker-compose run ${APP_IMG_NAME} django-admin.py startproject $APP_NAME_ACCOUNTS . 
    chown -R $USER_OWN:$USER_OWN $DIR_DJANGO/$APP_NAME_ACCOUNTS
fi

#init and update SASS style files (exec Compass) if required
if git diff HEAD~ --name-only | grep -qE "*.scss";
then
    echo "  ***  [INFO] SCSS styles: changed, recreating css  ***  "
    #docker run --rm -t -u root -v $PWD:/src -w /src bhrutledge/compass init --trace
    #docker run --rm -t -u root -v $PWD:/src -w /src bhrutledge/compass compile --trace

    docker run --rm -t -u root -v $PWD:/src -w /src compass-susy init
    docker run --rm -t -u root -v $PWD:/src -w /src compass-susy compile
else
    echo "  ***  [INFO] SCSS/CSS styles: no changes  ***  "
fi

#update searchform app database model
echo "  ***  [INFO] Django migrations: create and apply  ***  "
docker-compose run --rm ${APP_IMG_NAME} /code/manage.py makemigrations --trace
docker-compose run --rm ${APP_IMG_NAME} /code/manage.py migrate --trace
# check if migrations were really applied successfully. If not, try to migrate again and throw error
if [ $(docker-compose run --rm ${APP_IMG_NAME} /code/manage.py showmigrations | grep '\[ \]') ] {
   docker-compose run --rm ${APP_IMG_NAME} /code/manage.py migrate --trace
   echo "  *** [ERROR] Not all migrations were successfully applied! Have to throw error here!  ***  "
   echo "Error!" 1>&2
   exit 64
}


# build on windows
# docker-compose build --force-rm --no-cache

#create new app named shop
#docker-compose run web python manage.py startapp shop

#recompile sass from Windows
# docker run --rm -t -u root -v /$PWD://src -w //src compass-susy compile --trace

exit


 